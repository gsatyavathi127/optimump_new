import bannerImg from "../assets/banner.png";
function Banner() {
  return (
    <div className="Banner_wrapper">
      <div className="container">
        <div className="Banner_content">
          <div className="left_div">
            <h2>
              WE MAKE <br /> BATTERIES WORK
            </h2>
            <p>
              Maximize safety, performance, and longevity for your lithium
              battery with OptimumP Battery Management Systems
            </p>
            <button>Learn More</button>
          </div>
          <div className="right_div">
            <img src={bannerImg} className="home_banner_image"  />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Banner;
