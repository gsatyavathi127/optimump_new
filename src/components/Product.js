import React, { useState } from "react";
import bannerImg from "../assets/banner.png";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";

function Product() {
  const [value, setValue] = useState("1");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="product_main">
      <div className="product_hero_banner">
        <img src={bannerImg} className="product_hero_image" />
        <h2>OptimumP Products</h2>
      </div>
      <div>
        <TabContext
          value={value}
          TabIndicatorProps={{ style: { background: "#697ceb" } }}
        >
          <div className="main_tab_context">
            <div className="tabs_wrapper">
              <h4><strong>BMS Variants</strong></h4>
              <TabList
                onChange={handleChange}
                aria-label="lab API tabs example"
                orientation="vertical"
              >
                <Tab
                  label="Battery Management System"
                  value="1"
                  className="tab"
                />
                <Tab
                  label="Battery Monitoring System"
                  value="2"
                  className="tab"
                />
                <Tab label="Telemetric System" value="3" className="tab" />
                <Tab
                  label="Battery swapping Solution"
                  value="4"
                  className="tab"
                />
                <Tab
                  label="Thermal Managements System"
                  value="4"
                  className="tab"
                />
              </TabList>
            </div>
            <div className="tabs_content_wrapper">
              <TabPanel value="1" >
                <div className="tab_one_content">
                <div className="sub_content" >
                  <div>
                    <h2 className="tab_heading">Alpha-X</h2>
                  </div>
                  <div>
                    <h4 className="heading-29 _3tab">
                      <strong class="bold-text-7">Alpha-60, Alpha-100</strong>
                    </h4>
                  </div>
                  <div>
                    <div><strong>Specifications:</strong></div>
                    <div>
                      6-Cells to 16 Cells, CAN2.0B, Micro-SDCard, 4 Temp,Buzzer
                    </div>
                    <button className="learn_more_button" >Learn More</button>
                  </div>
                </div>
                <div  className="sub_content" >
                  <div className="tab_heading">Gamma</div>
                  <h4 className="heading-29 _3tab">
                      <strong class="bold-text-7">Gamma-400, Gamma-600,</strong>
                    </h4>
                  <div>
                    <div><strong>Specifications:</strong>  </div>
                    <div>32- 512 Cells, Contactor , Hall Probe Sensor</div>
                  </div>
                  <button className="learn_more_button" >Learn More</button>
                </div>
                <div  className="sub_content" >
                  <div className="tab_heading">Harari</div>
                  <div><strong>Description:</strong></div>
                  <div>
                    Harari is an Alpha model BMS parameter programming device.
                    which downloads its settings from BatInfo and stores it in
                    its memory permanently, untill reprogrammed.
                  </div>
                  <button className="learn_more_button" >Learn More</button>
                </div>
                <div  className="sub_content" >
                  <div className="tab_heading">Fujio</div>
                  <div><strong>Description:</strong></div>
                  <div>
                    Fujio is a wireless blackbox data logger compatible with
                    Alpha and Gamma model Bttery Manaagement System.
                  </div>
                  <button className="learn_more_button" >Learn More</button>
                </div>
                </div>
                <div className="tabs_content_wrapper"></div>
              </TabPanel>
              <TabPanel value="2">
                <div className="tab_two_content">
                  <div>
                    <h2 className="tab_heading"> Beta-Y (cell performance monitor)</h2>
                    </div>
                  <h3>Li-ion ,Ni-Cd, Li-Polymer, LTO </h3>
                  <div><strong>Description:</strong></div>
                  <div>
                    Beta is a highly integrated , high-accuracy wireless battery
                    monitoring system for 3 series to 512 series battery pack.
                    This system comes in Master-Slave configuration,where every
                    slave can be connected up-to 16 cells . A dedecated slave
                    for hall probe current sensor is used for high accurate
                    current measurments.
                  </div>
                  <button className="learn_more_button" >Learn More</button>
                </div>
              </TabPanel>
            <div className="tabs_content_wrapper"> </div>

              <TabPanel value="3">
                <div>
                  <div> 
                    <h2 className="tab_three_content">At Testing Phase, will Be updated soon.</h2>
                    
                    <button className="learn_more_button" >Learn More</button></div>
                </div>
              </TabPanel>
              <div className="tabs_content_wrapper"> </div>
              <TabPanel value="4">
                <div>
                  <div>
                    <h2 className="tab_four_content">At Testing Phase, will Be updated soon.</h2>
                    </div>
                    <button className="learn_more_button" >Learn More</button>
                </div>
              </TabPanel>
              <div className="tabs_content_wrapper"> </div>
            </div>
          </div>
        </TabContext>
      </div>
      <div className="battery_wrapper" >
        <img src={require('../assets/battery.png')}/>
      </div>
      <div className="battery_swapping_station_wrapper"> 
        <img src={require('../assets/Battery_Swapping Station.jpg')} />
      </div>
      <div className="product_end_footer_wrapper" >
        <img src={require('../assets/product_end_image1.jpg')}/>
        <img src={require('../assets/product_end_image2.png')}/>
      </div>

    </div>
  );
}

export default Product;
