import manageImg from "../assets/manage.png";

function Management() {
  return (
    <div className="manage_wrapper">
      <div className="container">
        <div className="manage_content">
          <div className="left_div">
            <img src={manageImg} />
          </div>
          <div className="right_div">
            <h2>Battery Management <br/> System</h2>
            <p>
              At OptimumP, we offer good quality Battery Management System
              (BMS). The <b>Battery Management System (BMS)</b> we offer are
              manufactured under strict quality controls and all the BMS go
              through high standard Quality Contol before reaching to Indian
              Market.
            </p>
            <p>
              We also deal in Smart Battery Management System. Our smart BMS is
              specially designed for rechargeable lithium battery packs to
              enhance performance of complete solutions. The communicative BMS
              monitors all cell's current and voltage fluctuations in pack and
              temperature of the pack.
            </p>
            <button>Learn More</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Management;
