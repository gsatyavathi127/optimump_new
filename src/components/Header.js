import logoImg from "../assets/logo_header.png";
import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="header_wrapper">
      <div className="header_content">
        <nav class="navbar navbar-expand-lg navbar-light navbar-fixed-top px-5">
          <Link class="navbar-brand" to="/">
            <img src={logoImg} />
          </Link>
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div
            class="collapse navbar-collapse justify-content-end"
            id="navbarNav"
          >
            <ul class="navbar-nav">
                <li class="nav-item">
                <Link to="/">Home</Link>
                </li>
                <li class="nav-item">
                <Link to="/product">Products</Link>
                </li>
                <li class="nav-item">
                <Link to="/apply">Apply</Link>
                </li>
                <li class="nav-item">
                <Link to="/about">About Us</Link>
                </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
