import logoImg from "../assets/logo.png";
import instaImg from "../assets/insta.png";
import linkedImg from "../assets/linkedin.png";

function Footer() {
  return (
    <div className="footer_wrapper px-5">
      
        <div className="footer_content">
          <div className="row">
            <div className="col-3">
              <div className="logo">
                <img src={logoImg} />
              </div>
              <div className="pt-3">
                <div className="footer-text">OptimumP Pvt.Ltd.</div>
                <div className="footer-text">Email- info@optimump.com</div>
                <div className="footer-adds">
                  H. No : 3-4-32, Vijayasree colony, Autonagar, Hyderabad-
                  500070India
                </div>
              </div>
            </div>
            <div className="col-3">
              <div className="link-head">Quick Links</div>
              <div>
                <ul>
                  <li>Home </li>
                  <li>Apply Job</li>
                  <li>About Us</li>
                </ul>
              </div>
            </div>
            <div className="col-3">
              <div  className="link-head">Products</div>
              <div>
                <ul>
                  <li>BMS</li>
                </ul>
              </div>
            </div>
            <div className="col-3">
              <div  className="link-head">Connect with US</div>
              <div>
                <ul>
                  <li><img src={instaImg} /> Instagram</li>
                  <li><img src={linkedImg} />Linkedin</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      
    </div>
  );
}

export default Footer;
