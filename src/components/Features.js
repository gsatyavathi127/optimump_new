import ImgOne from "../assets/f1.png";
import ImgTwo from "../assets/f2.png";
import Img3 from "../assets/f3.png";
import Img4 from "../assets/f4.png";
import Img5 from "../assets/f5.png";
import Img6 from "../assets/f6.png";

function Features() {
  return (
    <div className="feature_wrapper">
      <div className="Container-fluid">
        <div className="feature_content">
          <div className="feat_heading">Features</div>
          <div className="row p-0 m-0">
            <div className="col-md-3 py-3 ">
              <div className="header_box">
                <img src={ImgOne} />
                <h3>Over Voltage Protection</h3>
              </div>
              <div className="feat_text">
                Protects the battery pack from over-charging by terminating the
                charging process, when pack is fully charged
              </div>
            </div>
            <div className="col-md-3 py-3">
              <div className="header_box">
                <img src={ImgTwo} />
                <h3>Over Voltage Protection</h3>
              </div>
              <div className="feat_text">
                Protects the battery pack from over-charging by terminating the
                charging process, when pack is fully charged
              </div>
            </div>
            <div className="col-md-3 py-3">
              <div className="header_box">
                <img src={Img3} />
                <h3>Over Voltage Protection</h3>
              </div>
              <div className="feat_text">
                Protects the battery pack from over-charging by terminating the
                charging process, when pack is fully charged
              </div>
            </div>
            <div className="col-md-3 py-3">
              <div className="header_box">
                <img src={Img4} />
                <h3>Over Voltage Protection</h3>
              </div>
              <div className="feat_text">
                Protects the battery pack from over-charging by terminating the
                charging process, when pack is fully charged
              </div>
            </div>
            <div className="col-md-3 py-3">
              <div className="header_box">
                <img src={Img5} />
                <h3>Over Voltage Protection</h3>
              </div>
              <div className="feat_text">
                Protects the battery pack from over-charging by terminating the
                charging process, when pack is fully charged
              </div>
            </div>
            <div className="col-md-3 py-3">
              <div className="header_box">
                <img src={Img6} />
                <h3>Over Voltage Protection</h3>
              </div>
              <div className="feat_text">
                Protects the battery pack from over-charging by terminating the
                charging process, when pack is fully charged
              </div>
            </div>
            <div className="col-md-3 py-3">
              <div className="header_box">
                <img src={ImgOne} />
                <h3>Over Voltage Protection</h3>
              </div>
              <div className="feat_text">
                Protects the battery pack from over-charging by terminating the
                charging process, when pack is fully charged
              </div>
            </div>
            <div className="col-md-3 py-3">
              <div className="header_box">
                <img src={ImgOne} />
                <h3>Over Voltage Protection</h3>
              </div>
              <div className="feat_text">
                Protects the battery pack from over-charging by terminating the
                charging process, when pack is fully charged
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Features;
