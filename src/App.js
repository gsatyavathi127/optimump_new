import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import "./Style.scss";
import Navbar from "./components/Header";
import Footer from "./components/Footer";
import About from "./components/AboutUs";
import Apply from "./components/Apply";
import Home from "./components/Home";
import Product from "./components/Product";
import { Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact={true} path="/">
          <Home />
        </Route>
        <Route exact={true} path="/product">
          <Product />
        </Route>
        <Route exact={true} path="/apply">
          <Apply />
        </Route>
        <Route exact={true} path="/about">
          <About />
        </Route>
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
